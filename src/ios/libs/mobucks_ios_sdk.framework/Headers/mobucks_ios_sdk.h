//
//  mobucks_ios_sdk.h
//  mobucks-ios-sdk
//
//  Created by OTM User.
//  Copyright © 2018 otm. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for mobucks_ios_sdk.
FOUNDATION_EXPORT double mobucks_ios_sdkVersionNumber;

//! Project version string for mobucks_ios_sdk.
FOUNDATION_EXPORT const unsigned char mobucks_ios_sdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <mobucks_ios_sdk/PublicHeader.h>
