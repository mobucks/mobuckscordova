import UIKit
import mobucks_ios_sdk
@objc(MobucksCordovaWrapper) class MobucksCordovaWrapper : CDVPlugin{
var bannerView: MBBannerAdView?

var uid: String?
var password: String?
var bannerWidth: Double?
var bannerHeight: Double?
var plid: String?
var position: Double?
var GoogleUid: String?

@objc(setConfigs:)
func setConfigs(command: CDVInvokedUrlCommand) {
    var pluginResult = CDVPluginResult(
        status: CDVCommandStatus_ERROR
    )

   let size = command.arguments.capacity

   let uidMeth = command.arguments[0] as? String ?? ""

   let passwordMeth = command.arguments[1] as? String ?? ""

   let GoogleUidMeth = command.arguments[2] as? String ?? ""

    if size > 0 {
        if ( uidMeth.characters.count > 0 && passwordMeth.characters.count > 0 ){

           uid = uidMeth
           password = passwordMeth

            if(GoogleUidMeth.characters.count > 0){
                GoogleUid = GoogleUidMeth
            }else{
                GoogleUid = ""
            }

            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: "ALL OK"
            )
        }

    }

    self.commandDelegate!.send(
        pluginResult,
        callbackId: command.callbackId
    )
}

@objc(loadBanner:)
func loadBanner(command: CDVInvokedUrlCommand) {

    var pluginResult = CDVPluginResult(
        status: CDVCommandStatus_ERROR
    )

    let size = command.arguments.capacity

    let plidMeth = command.arguments[0] as? String ?? ""

    let width = command.arguments[1] as? String ?? ""
    let height = command.arguments[2] as? String ?? ""
    let pos = command.arguments[3] as? String ?? ""

    if size > 0 {
        if ( plidMeth.characters.count > 0 && height.characters.count > 0 && width.characters.count > 0){

            plid = plidMeth
            bannerWidth = Double(width)
            bannerHeight = Double(height)
            position=Double(pos)

            let  screenRect  = UIScreen.main.bounds
            let  centerX     = (screenRect.size.width / 2) - 150
            let  centerY     = (screenRect.size.height / 2) - 25

            let cordovaRect = self.webView.bounds
            let centerCordX = (cordovaRect.size.width/2) - CGFloat(bannerWidth!/2)
            var  centerCordY = cordovaRect.size.height - CGFloat(bannerHeight!)

            let positionY = (CGFloat(position!)*centerCordY)/100
            // Set up some sizing variables we'll need when we create our ad view.
            var rect  = CGRect(x:Int(centerCordX), y:Int(positionY), width:Int(bannerWidth!), height:Int(bannerHeight!))
            //var rect  = CGRect(x:centerX, y:centerY, width:320, height:50)
            var size  = CGSize(width:bannerWidth!, height:bannerHeight!)

            // Create the banner viewsssss
            bannerView =  MBBannerAdView(frame: rect, placementId:plid!,uid:uid!,password:password!,adSize:size)
            bannerView?.addGoogleToRoot(viewController: self.viewController)
            bannerView?.addGoogleUnitId(GoogleUid!)                /*
             Called when ad is successfully loaded
             */

            //banner on top

            bannerView?.onAdLoaded { (uiView:UIView) in

            }
            /*
             Called when ad is clicked
             */
            bannerView?.onAdViewClicked { (uiView:UIView) in
            }
            /*
             Called when ad failed to load
             */
            bannerView?.onAdFailed{ (error:Error) in
                print(error)
            }

            self.webView.addSubview(bannerView!)
            self.webView.bringSubviewToFront(bannerView!)
            //bannerView?.isHidden = false
            bannerView!.loadAd()
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: "ALL OK"
            )
        }

    }

    self.commandDelegate!.send(
        pluginResult,
        callbackId: command.callbackId
    )

}
}
