package cordova.plugin.mobucks;

import android.accessibilityservice.AccessibilityService;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.icu.text.SymbolTable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;
import com.mobucks.androidsdk.interfaces.AdListener;
import com.mobucks.androidsdk.models.Ad;
import com.mobucks.androidsdk.views.BannerAdView;
import org.apache.cordova.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static org.apache.cordova.device.Device.TAG;

/**
 * This class echoes a string called from JavaScript.
 */
public class MobucksCordovaWrapper extends CordovaPlugin {

    private static String plid;
    private static  String password;
    private static String uid;
    private String width;
    private String height;
    private  RelativeLayout adViewLayout;
    private String offset;

    BannerAdView bannerAd;
    private boolean bannerAtTop = false;

    private static String googleId;

    private int widthP;
    private int heightP;


    private boolean configsAreSet = false;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);

    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        if (action.equals("loadBanner")) {

            this.loadMobucks(args.getJSONObject(0), callbackContext);
            return true;
        }

        if (action.equals("removeBanner")) {

            this.destroyAd(callbackContext);
            return true;

        }

        if (action.equals("setConfigs")) {

            this.setConfigs(args.getJSONObject(0), callbackContext);
            return true;
        }

        return false;
    }

    private PluginResult loadMobucks(JSONObject credentials, final CallbackContext callbackContext) {

        if (credentials != null && configsAreSet) {


            width = "";
            height = "";
            plid = "";
            offset = "";

            try {

                width = credentials.getString("width");
                height = credentials.getString("height");
                plid = credentials.getString("plid");
                offset = credentials.getString("offset");

                if (credentials.getString("place") != null) {
                    if (credentials.getString("place").equals("top")) {
                        bannerAtTop = true;
                    } else if (credentials.getString("place").equals("bottom")) {
                        bannerAtTop = false;
                    }
                }

            } catch (Exception e) {
                e.toString();
            }

            cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    boolean isForLoad = false;

                    if (bannerAd == null) {

                        bannerAd = new BannerAdView(cordova.getActivity());
                        bannerAd.setMaxSize(Integer.parseInt(width), Integer.parseInt(height));
                        bannerAd.setUid(uid);
                        bannerAd.setPassword(password);
                        bannerAd.setPlacementId(plid);
                        bannerAd.setGoogleUnit(googleId);
                        bannerAd.setVisibility(View.VISIBLE);
                        isForLoad = true;

                    }

                    if (bannerAd.getParent() != null) {
                        ((ViewGroup) bannerAd.getParent()).removeView(bannerAd);
                    }

                    if (isForLoad) {
                        bannerAd.loadAd();
                    }

                    bannerAd.setAdListener(new AdListener<BannerAdView>() {
                        @Override
                        public void onAdloaded(BannerAdView adView) {
                            Log.i("-- Banner --", "LOADED");
                            System.out.println("loaded ------------------------ > " + adView.getWidth()+","+adView.getHeight());
                            System.out.println("IS GOOGLE AD ------------------------ > " + adView.isGoogleAd());


                            Rect localRect = new Rect();
                            adViewLayout.getGlobalVisibleRect(localRect);
                            float dpi = ((float) webView.getContext().getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
                            System.out.println("GET TOP ------------------------ > " + adViewLayout.getBottom());

                            if(bannerAd.isGoogleAd() || bannerAd.isAppNexusAd()) {
                                int paddingCenter = (int) Math.ceil((widthP - Math.ceil(Float.parseFloat(width)) * dpi) / 2);
                                int maxHeight = heightP - localRect.top - (int) Math.ceil(Float.parseFloat(height) * dpi);
                                bannerAd.setPadding(paddingCenter, (int) Math.ceil((Float.parseFloat(offset) * maxHeight) / 100), 0, 0);
                                int middle = (widthP - 350)/2;

                            }else{
                                int paddingCenter =  (int)Math.ceil((widthP-Integer.parseInt(width))/2);
                                int heightScr = adViewLayout.getBottom() -adViewLayout.getTop() - (int) (Math.floor(Float.parseFloat(height)) * dpi);

                                bannerAd.setPadding(0,(int) Math.floor((Float.parseFloat(offset) * heightScr) / 100),0,0);

                            }
                        }

                        @Override
                        public void onAdFailed(Exception e) {
                            Log.i("-- Banner --", "FAILED TO LOAD");
                        }

                        @Override
                        public void onAdClicked(BannerAdView adView) {
                            Log.i("-- Banner --", "BANNER CLICKED");
                        }
                    });


                            Point point = new Point();
                            Display displaySize = cordova.getActivity().getWindowManager().getDefaultDisplay();
                            displaySize.getSize(point);

                            widthP = point.x;
                            heightP = point.y;


                            adViewLayout = new RelativeLayout(cordova.getActivity());
                            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);



                            try {
                                ((ViewGroup) (((View) webView.getClass().getMethod("getView").invoke(webView)).getParent()))
                                        .addView(adViewLayout, params);
                            } catch (Exception e) {
                                ((ViewGroup) webView).addView(adViewLayout, params);
                            }


                            adViewLayout.addView(bannerAd);

                            adViewLayout.bringToFront();




                    callbackContext.success();
                }

            });
        } else {

            callbackContext.error("Something went veery wrong... There are no credentials :(");
        }

        return null;
    }


    private PluginResult destroyAd(final CallbackContext callbackContext) {

        cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final CallbackContext delayCallback = callbackContext;
                if (bannerAd != null) {
                    bannerAd.destroy();
                    bannerAd = null;
                }
                if (adViewLayout != null) {
                    ViewGroup parentView = (ViewGroup) adViewLayout.getParent();
                    if (parentView != null) {
                        parentView.removeView(adViewLayout);
                    }
                    adViewLayout = null;
                }

            }
        });

        return null;
    }

    private void setConfigs(JSONObject configs, final CallbackContext callbackContext) {

        if (configs != null) {

            googleId = "";
            password = "";
            uid = "";

            try {


                uid = configs.getString("uid");
                password = configs.getString("password");
                googleId = configs.getString("GoogleUid");

            } catch (Exception e) {
                e.toString();
            }

            if (!googleId.equals("")) {
                configsAreSet = true;
            }

        }

    }

}
