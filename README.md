# GUIDE TO USE MOBUCKS CORDOVA PLUGIN

## 1) CREATE AN IONIC PROJECT WITH CORDOVA OR USE AN EXISTING ONE. 
        
CREATE A NEW ONE: 
  
```bash
ionic start <name> <template> --cordova [other options]
```  
## 2) ADD ANDROID PLATFORM

```bash
cordova platform add android
```     

## 3)ADD THE PLUGIN TO YOUR PROJECT

```bash
ionic cordova plugin add git+https://mobucks@bitbucket.org/mobucks/mobuckscordova.git --save
```   

## 4)YOU CAN CALL THE PLUGIN WITH THE FOLLOWING DECLARATION:

```ts
declare var cordova: any; //<--- add this above imports
cordova.plugins.MobucksCordovaWrapper.<METHOD>()
```     

## 5)THE PLUGIN HAS THREE(3) METHODS:
    

### a)setConfigs()
    
parameters: 1)A JSON object with three(3) fields uid, password and googleUnitId.
            2)A success function
            3)An error function
        
        
example: 

```ts
        const params = {
                  GoogleUid: "/6499/example/banner"
                  uid: 'vodacomsa',
                  password: 'bd3156813XXXXXXXXXXXX'//X can be number or character. Last 3 are banner configurations
                  };
                  
                  cordova.plugins.MobucksCordovaWrapper.setConfigs(params,function(res){},function(err) { });
``` 
    

### b)loadBannerMethod()
    
parameters: 1)A JSON object with three(3) fields width, height, position and the plid
            2)A success function
            3)An error function
                    
example:     

```ts
            const obj = {
                    width: '350',//weight of banner
                      height: '50' //height of the banner
                      plid: '785',
                      position:'X'// Where X is a number between 0-100
                      };
                       
                    cordova.plugins.MobucksCordovaWrapper.loadBanner(obj,function(res) {},function(err) { });
```               

Position attr place the banner accordingly to number X and the screen. "0" places the banner on the top, "100" places the banner at the top and "50" in the middle etc.

### c)removeBanner()
    
parameters: 1)A success function
            2)An error function
        
example: 

```ts
        cordova.plugins.MobucksCordovaWrapper.removeBanner(function(res) {},function(err) { });
```  

!! there is not same function for ios...

### !!!!method a)setConfigs must be called first.

#Instruction for Ionic ios

### 1)Steps 1,3,4 are the same as the android except step 2.

### 2)Step 2 is: ADD IOS PLATFORM

```bash
cordova platform add ios
```

#### 2)Set Swift Version and Google Ad Manager Key

To set the proper Swift Version and the Google Ad Manager configurations you have to add the following lines in the config.xml file of your project.

You have to place them inside the platform name='ios' section.

```angular2html
<config-file parent="GADIsAdManagerApp" target="*-Info.plist">
            <true />
 </config-file>
 <preference name="UseSwiftLanguageVersion" value="4.2" />
```

The config file adds the Google Conf to the projects_Info.plist and
the preference uses the cordova-plugin-add-swift-support Plugin to set the Swift_version automatically.

To be sure the these configurations are set remove and add again the ios platform to your project.

```bash
ionic cordova platform rm ios
ionic cordova platform add ios
```

#### 5)For ios there are TWO(2) METHODS:

##### a)setIosConfigs()

parameters: Its a JSON object with uid,password and Google Unit Id.

example:     

```ts
            var configs = {
                        uid:"XXXX",
                        password:"XXXXX",
                        GoogleUid:"/6499/example/banner"        
                    }

             cordova.plugins.MobucksCordovaWrapper.setIosConfigs(configs, success_function, error_function)
                    
```               

##### b)loadIosBanner()

parameters: Its a JSON object with plid(Placement Id), width, height and the position. Position is a number between 0-100 which defines where the banner must be placed based on height in the screen.
If position:0 the banner is placed on top, if position:100 is placed at bottom and if position is 50 is places in the middle.

example:     

```ts
            var params = {
                        plid:"XXXX",
                        width:"XXXXX",
                        height:"XXXXXXX"
                        position:"X"        
                    }

             cordova.plugins.MobucksCordovaWrapper.loadIosBanner(params, success_function, error_function)
                    
```    

