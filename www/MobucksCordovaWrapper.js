

var exec = require("cordova/exec");

function getOS() {
  var userAgent = window.navigator.userAgent,
      platform = window.navigator.platform,
      macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
      windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
      iosPlatforms = ['iPhone', 'iPad', 'iPod'],
      os = null;

  if (macosPlatforms.indexOf(platform) !== -1) {
    os = 'Mac OS';
  } else if (iosPlatforms.indexOf(platform) !== -1) {
    os = 'iOS';
  } else if (windowsPlatforms.indexOf(platform) !== -1) {
    os = 'Windows';
  } else if (/Android/.test(userAgent)) {
    os = 'Android';
  } else if (!os && /Linux/.test(platform)) {
    os = 'Linux';
  }

  return os;
}


if (getOS() == 'Android'){

  exports.setConfigs = function(jsonObj, success, error) {

      exec(success, error, "MobucksCordovaWrapper", "setConfigs", [jsonObj]);

  };


  exports.loadBanner = function(jsonObj, success, error) {

      exec(success, error, "MobucksCordovaWrapper", "loadBanner", [jsonObj]);
  };

  exports.removeBanner = function(jsonObj, success, error) {
    exec(success, error, "MobucksCordovaWrapper", "removeBanner", [jsonObj]);
  };

}else if(getOS() == 'iOS'){

  exports.setConfigs = function(configs, success, error) {


      var confJSON = JSON.stringify(configs);
      var confObj = JSON.parse(confJSON);
      exec(success, error, "MobucksCordovaWrapper", "setConfigs", [confObj.uid, confObj.password,confObj.GoogleUid]);

  };

  exports.loadBanner = function(params, success, error) {

      var paramJSON = JSON.stringify(params);
      var paramObj = JSON.parse(paramJSON);
      exec(success, error, "MobucksCordovaWrapper", "loadBanner", [paramObj.plid, paramObj.width, paramObj.height,paramObj.position]);

  };
}


/*exports.loadBanner = function(jsonObj, success, error) {

  if (getOS() == 'Android'){
    exec(success, error, "MobucksCordovaWrapper", "loadBanner", [jsonObj]);
  }else if (getOS() == 'iOS'){
    var paramJSON = JSON.stringify(params);
    var paramObj = JSON.parse(paramJSON);
    exec(success, error, "MobucksCordovaWrapper", "loadBanner", [paramObj.plid, paramObj.width, paramObj.height,paramObj.position]);
  }

};

exports.removeBanner = function(jsonObj, success, error) {
  exec(success, error, "MobucksCordovaWrapper", "removeBanner", [jsonObj]);
};

exports.setConfigs = function(jsonObj, success, error) {

  if (getOS() == 'Android'){
    exec(success, error, "MobucksCordovaWrapper", "setConfigs", [jsonObj]);
  }else if(getOS() == 'iOS'){
    var confJSON = JSON.stringify(configs);
    var confObj = JSON.parse(confJSON);
    exec(success, error, "MobucksCordovaWrapper", "setConfigs", [confObj.uid, confObj.password,confObj.GoogleUid]);
  }


};

exports.setIosConfigs = function(configs, success, error){
  var confJSON = JSON.stringify(configs);
  var confObj = JSON.parse(confJSON);
  exec(success, error, "MobucksCordovaWrapper", "setIosConfigs", [confObj.uid, confObj.password,confObj.GoogleUid]);
};

exports.loadIosBanner = function(params, success, error){
  var paramJSON = JSON.stringify(params);
  var paramObj = JSON.parse(paramJSON);
  exec(success, error, "MobucksCordovaWrapper", "loadIosBanner", [paramObj.plid, paramObj.width, paramObj.height,paramObj.position]);
};*/
